## Bibcite OAI-PMH DC format

Extend the bibcite module to allow importing Dublin Core data in XML-format originating from an OAI-PMH repository.

This is intended to be a temporary repository.
Hopefully we can move this into the bibcite module at some point in the future.
