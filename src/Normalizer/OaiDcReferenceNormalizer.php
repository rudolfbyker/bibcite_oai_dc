<?php

namespace Drupal\bibcite_oai_dc\Normalizer;

use Drupal\bibcite_entity\Entity\ReferenceInterface;
use Drupal\bibcite_entity\Normalizer\ReferenceNormalizerBase;

/**
 * Normalizes/denormalizes reference entity to OAI DC format.
 */
class OaiDcReferenceNormalizer extends ReferenceNormalizerBase {

  /**
   * {@inheritdoc}
   */
  public function denormalize($data, $class, $format = NULL, array $context = []) {

    // Try to find an ISBN number in the identifier.
    if (!empty($data['identifier']) && is_array($data['identifier'])) {
      foreach ($data['identifier'] as $value) {
        if (FALSE !== stripos($value, 'isbn')) {
          $data['isbn'] = $value;
        }
        elseif (FALSE !== stripos($value, 'issn')) {
          $data['issn'] = $value;
        }
        elseif (FALSE !== stripos($value, 'http')) {
          $data['url'] = $value;
        }
        else {
          \Drupal::logger('bibcite_oai_dc')->warning(
            t(
              "Dropping identifier, since we don't know how to interpret it: @identifier",
              ['@identifier' => $value]
            )
          );
          echo $value;
        }
      }
    }

    // Some fields may only have one value. Use the first one.
    foreach ([
      'publisher',
      'date',
      'year',
      'language',
      'description',
    ] as $key) {
      if (!empty($data[$key]) && is_array($data[$key])) {
        $data[$key] = reset($data[$key]);
      }
    }

    // If the year of publication is not set, try to get it from the date.
    if (empty($data['year']) && !empty($data['date'])) {
      $info = date_parse($data['date']);
      $data['year'] = $info['year'];
    }

    return parent::denormalize($data, $class, $format, $context);
  }

}
