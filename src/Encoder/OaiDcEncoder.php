<?php

namespace Drupal\bibcite_oai_dc\Encoder;

use SimpleXMLElement;
use Symfony\Component\Serializer\Encoder\DecoderInterface;
use Symfony\Component\Serializer\Encoder\EncoderInterface;

/**
 * OAI DC format encoder.
 */
class OaiDcEncoder implements EncoderInterface, DecoderInterface {

  /**
   * The format that this encoder supports.
   *
   * @var string
   */
  protected static $format = 'oai_dc';

  /**
   * @param $xml_record
   *
   * @return array
   */
  public static function decode_one($xml_record): array {
    $record = [];

    // Split the header into separate fields.
    $header = $xml_record['children']['header'][0] ?? [];
    $record['oai_status'] = $header['attributes']['status'] ?? NULL;
    $record['oai_identifier'] = $header['children']['identifier'][0]['text'] ?? NULL;
    $record['oai_datestamp'] = $header['children']['datestamp'][0]['text'] ?? NULL;

    $dc = $xml_record['children']['metadata'][0]['children']['oai_dc:dc'][0]['children'] ?? [];
    foreach ($dc as $property) {
      foreach ($property as $value) {
        $name = $value['name'];
        $text = $value['text'];
        $record[$name][] = $text;
      }
    }

    if (empty($record['type'])) {
      // Type must be set, to prevent exceptions later.
      $record['type'] = 'unknown';
    }
    elseif (is_array($record['type'])) {
      // Type must not be an array.
      $record['type'] = $record['type'][0];
    }

    // Title must not be an array. If there are multiple parts, join them.
    if (empty($record['title'])) {
      $record['title'] = 'untitled';
    }
    elseif (is_array($record['title'])) {
      $record['title'] = implode(" ", $record['title']);
    }

    if (!empty($record['creator']) && is_array($record['creator'])) {
      // Remove large portions of whitespace in the creators
      foreach ($record['creator'] as &$creator) {
        $creator = preg_replace('/\s+/', ' ', $creator);
      }

      // Try to remove duplicate creators.
      $record['creator'] = self::deduplicateArrayOfStrings($record['creator']);
    }

    // Try to remove duplicate identifiers.
    if (!empty($record['identifier']) && is_array($record['identifier'])) {
      $record['identifier'] = self::deduplicateArrayOfStrings($record['identifier']);
    }

    // Try to remove duplicate keywords.
    if (!empty($record['subject']) && is_array($record['subject'])) {
      $record['subject'] = self::deduplicateArrayOfStrings($record['subject']);
    }

    return $record;
  }

  /**
   * Removes duplicate elements from string arrays.
   *
   * Example:
   * - Input: ["Schutte, G. J. (Gerrit Jan), 1940-", "Schutte, G. J."]
   * - Output: ["Schutte, G. J. (Gerrit Jan), 1940-"]
   *
   * @param array $array
   *   The array of strings to deduplicate.
   *
   * @return array
   *   A new array of strings, sorted from longest to shortest, with duplicates
   *   removed.
   */
  private static function deduplicateArrayOfStrings(array $array) {

    // First make sure the array of strings is sorted longest to shortest.
    usort($array, function($a, $b) {
      return strlen($b) - strlen($a);
    });

    $new_values = [];
    foreach ($array as $value) {
      if (!empty($value)) {
        // Only add the value if it doesn't appear in one of the existing values.
        if (!self::findStringInArray($value, $new_values)) {
          $new_values[] = $value;
        }
      }
    }

    return $new_values;
  }

  /**
   * Check if any of the array elements (strings) contain the given string.
   *
   * @param string $needle
   *   The string to search for.
   * @param array $haystack
   *   The array of strings to search through.
   *
   * @return bool
   *   TRUE if at least one array element contains the string.
   */
  private static function findStringInArray(string $needle, array $haystack) {
    foreach ($haystack as $value) {
      if (FALSE !== strpos($value, $needle)) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function supportsDecoding($format) {
    return $format == static::$format;
  }

  /**
   * {@inheritdoc}
   */
  public function decode($data, $format, array $context = []) {
    if (is_string($data)) {
      $xmlElement = new SimpleXMLElement($data);
    }
    elseif (is_a($data, SimpleXMLElement::class)) {
      $xmlElement = $data;
    }
    else {
      throw new \Exception("Data must be an XML string or a SimpleXMLElement.");
    }
    $arr = self::xmlObjToArr($xmlElement);
    $records = [];

    // Detect whether the entire XML or just the <record> was given.
    if (isset($arr['children']['listrecords'])) {
      // Entire XML, with many records.
      foreach ($arr['children']['listrecords'][0]['children']['record'] as $xml_record) {
        $records[] = self::decode_one($xml_record);
      }
    }
    elseif ($arr['name'] == 'record') {
      // One record.
      $records[] = self::decode_one($arr);
    }
    else {
      throw new \Exception("Could not decode the XML.");
    }

    return $records;
  }

  /**
   * Convert a given SimpleXMLElement object into an array.
   *
   * Preserves namespaces and attributes.
   * From http://php.net/manual/en/book.simplexml.php#108688
   *
   * @param $obj SimpleXmlElement
   *   The object to decode.
   *
   * @return array
   *   An array which represents all info that could be scraped from the XML.
   */
  public static function xmlObjToArr($obj) {
    $namespace = $obj->getDocNamespaces(true);
    $namespace[NULL] = NULL;

    $children = array();
    $attributes = array();
    $name = strtolower((string)$obj->getName());

    $text = trim((string)$obj);
    if( strlen($text) <= 0 ) {
      $text = NULL;
    }

    // get info for all namespaces
    if(is_object($obj)) {
      foreach( $namespace as $ns=>$nsUrl ) {
        // atributes
        $objAttributes = $obj->attributes($ns, true);
        foreach( $objAttributes as $attributeName => $attributeValue ) {
          $attribName = strtolower(trim((string)$attributeName));
          $attribVal = trim((string)$attributeValue);
          if (!empty($ns)) {
            $attribName = $ns . ':' . $attribName;
          }
          $attributes[$attribName] = $attribVal;
        }

        // children
        $objChildren = $obj->children($ns, true);
        foreach( $objChildren as $childName=>$child ) {
          $childName = strtolower((string)$childName);
          if( !empty($ns) ) {
            $childName = $ns.':'.$childName;
          }
          $children[$childName][] = self::xmlObjToArr($child);
        }
      }
    }

    return array(
      'name'=>$name,
      'text'=>$text,
      'attributes'=>$attributes,
      'children'=>$children
    );
  }

  /**
   * {@inheritdoc}
   */
  public function supportsEncoding($format) {
    return $format == static::$format;
  }

  /**
   * {@inheritdoc}
   */
  public function encode($data, $format, array $context = []) {

    return "";
  }

}
