<?php

namespace Drupal\Tests\bibcite_oai_dc\Kernel;

use Drupal\bibcite_oai_dc\Encoder\OaiDcEncoder;
use Drupal\Tests\bibcite_import\Kernel\FormatDecoderTestBase;

/**
 * @coversDefaultClass \Drupal\bibcite_oai_dc\Encoder\OaiDcEncoder
 * @group bibcite
 */
class OaiDcDecodeTest extends FormatDecoderTestBase {

  /**
   * Modules to install.
   *
   * @var array
   */
  public static $modules = [
    'system',
    'user',
    'serialization',
    'bibcite',
    'bibcite_entity',
    'bibcite_oai_dc',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();

    $this->installConfig(OaiDcDecodeTest::$modules);

    $this->encoder = new OaiDcEncoder();
    $this->format = 'oai_dc';
    $this->resultDir = __DIR__ . '/../../data/decoded';
    $this->inputDir = __DIR__ . '/../../data/encoded';
  }

}
